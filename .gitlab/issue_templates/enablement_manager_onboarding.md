## [First Name], Welcome to GitLab and the Enablement Section

We are all excited that you are joining us as a manager in the [Enablement section](https://about.gitlab.com/handbook/engineering/development/enablement). You should have already received an onboarding issue from the [People Group](https://about.gitlab.com/handbook/people-group/) as well as a new manager onboarding issue.  This onboarding issue is specific to Engineering Managers in the Enablement Section.

This tasks in this issue are grouped by responsible individual(s).  Just focus on "New Team Member" items, and feel free to reach out to your manager or peer managers if you have any questions.

### New Team Member
* [ ] Join the following Slack channels:
  * [ ] [#backend-hiring](https://gitlab.slack.com/messages/C393D69L0)
  * [ ] [#engineering-fyi](https://gitlab.slack.com/messages/CJWA4E9UG)
  * [ ] [#s_enablement](https://gitlab.slack.com/messages/CMPK8QBB9)
  * [ ] [#ceo](https://gitlab.slack.com/messages/C3MAZRM8W) (suggested)
  * [ ] [#cto](https://gitlab.slack.com/messages/C9X79MNJ3) (suggested)
  * [ ] [#vp-development](https://gitlab.slack.com/messages/C9X79MNJ3) (suggested)
* [ ] Take ownership of team handbook page under [Enablement section](https://about.gitlab.com/handbook/engineering/development/enablement/)
  * Example: https://about.gitlab.com/handbook/engineering/development/enablement/data_stores/database/
* [ ] Learn about the Incident Manager process.
     * [ ] Read https://about.gitlab.com/handbook/engineering/infrastructure/incident-management 
     * [ ] Read https://about.gitlab.com/handbook/engineering/infrastructure/incident-manager-onboarding/

#### Starting a New Team

These tasks are for EMs who are joining a newly formed team.

* [ ] Create your team channel if it does not yet exist. #g_<team name>, example: #g_geo
* [ ] Register an [access request](https://about.gitlab.com/handbook/business-ops/it-ops-team/access-requests/) to establish a sub-group on GitLab (if it does not exist). [Example](https://gitlab.com/gitlab-com/access-requests/issues/2087)
* [ ] After sub-group is established, create projects for team-specific issues, onboarding, etc. See other team sub-groups for examples:
  * [Database Team](https://gitlab.com/gitlab-org/database-team)
  * [Distribution Team](https://gitlab.com/gitlab-org/distribution)
* [ ] Review or create team onboarding template. This should be a template specific to your team, not the general GitLab onboarding template. Examples:
  * [Distribution Team Template](https://gitlab.com/gitlab-org/distribution/team-tasks/blob/master/.gitlab/issue_templates/Team-onboarding.md)
  * [Database Team Template](https://gitlab.com/gitlab-org/database-team/team-tasks/-/blob/master/.gitlab/issue_templates/onboarding.md)

### Manager
Add new hire to:
* [ ] Google Group enablement-section@gitlab.com
* [ ] Enablement Staff Meeting
* [ ] Enablement/AppSec Monthly sync
* [ ] [gitlab-org/enablement-section group and relevant sub-groups](https://gitlab.com/gitlab-org/enablement-section)
* [ ] Slack channels:
  * [ ] [#fe-hiring-management](https://gitlab.slack.com/messages/GE2MG5M4H)
  * [ ] [#enablement-hiring](https://gitlab.slack.com/messages/GJMN3EK4Y)
  * [ ] [#enablement-leaders](https://gitlab.slack.com/messages/GMLB4U8EN)
* [ ] Member of [@gitlab-com/people-group/hiring-processes](https://gitlab.com/gitlab-com/people-group/hiring-processes/tree/master/Engineering)
* [ ] Request to be added to group triage reports if applicable. [Example](https://gitlab.com/gitlab-org/gitlab/issues/33531)
* [ ] Add to [https://gitlab.com/groups/gl-technical-interviews/backend/-/group_members](https://gitlab.com/groups/gl-technical-interviews/backend/-/group_members) as Owner
* [ ] Add to [Enablement weekly async updates](https://gitlab.com/gitlab-org/enablement-section/enablement-status-update). [Example](https://gitlab.com/gitlab-org/enablement-section/enablement-status-update/-/merge_requests/6)
* [ ] Add to [Next Planning Template](https://gitlab.com/gitlab-org/enablement-section/discussions/-/blob/main/.gitlab/issue_templates/next-planning.md)
* [ ] Ensure `Job Title Specialty` is correct in Workday

/confidential
/due in 14 days
